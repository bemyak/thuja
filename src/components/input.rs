//! Simple one-line text input component

use std::{cell::RefCell, cmp::min};

use crossterm::event::{KeyCode, KeyModifiers};
use tui::{
    layout::{Constraint, Direction, Layout},
    style::{Modifier, Style},
    text::{Span, Spans, Text},
    widgets::{Block, Borders, Paragraph},
};

use crate::{Component, Subscription};

#[derive(Debug, PartialEq)]
pub enum Msg {
    Input(char),
    DelWordBackward,
    DelWordForward,
    MoveWordBackward,
    MoveWordForward,
    Backspace,
    Del,
    Left,
    Right,
    Home,
    End,
}

#[derive(Debug, Default)]
pub struct Input {
    pub text: String,
    pub placeholder: String,
    pub title: String,
    is_focused: bool,
    cur_pos: usize,
    scroll: RefCell<u16>,
}

impl Input {
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }
    #[must_use]
    pub fn with_text<S: Into<String>>(mut self, text: S) -> Self {
        self.text = text.into();
        self
    }
    #[must_use]
    pub fn with_placeholder<S: Into<String>>(mut self, placeholder: S) -> Self {
        self.placeholder = placeholder.into();
        self
    }

    #[must_use]
    pub fn with_title<S: Into<String>>(mut self, title: S) -> Self {
        self.title = title.into();
        self
    }
}

impl Component for Input {
    type Message = Msg;

    fn view(&self, frame: &mut tui::Frame<'_, crate::ThujaBackend>, area: tui::layout::Rect) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
            .split(area);
        let area = chunks[0];

        let mut prev_scroll = self.scroll.borrow_mut();
        let text = self.get_text();
        let cur_pos = self.cur_pos as u16;
        let border = match area.height {
            0 => return,
            1 => false,
            _ => true,
        };

        let l = if border {
            area.width.checked_sub(3).unwrap_or_default()
        } else {
            area.width.checked_sub(1).unwrap_or_default()
        };

        let bounds = (*prev_scroll, *prev_scroll + l);
        let scroll = if cur_pos < bounds.0 {
            cur_pos
        } else if cur_pos > bounds.1 {
            cur_pos - l
        } else {
            *prev_scroll
        };

        // Shift scroll left is the cursor is close to border
        let outer_margin = (
            scroll,
            (self.text.len() as u16)
                .checked_sub(scroll + l)
                .unwrap_or_default(),
        );
        let inner_margin = (cur_pos - scroll, scroll + l - cur_pos);

        let scroll = if outer_margin.0 > 0 && inner_margin.0 < 3 {
            outer_margin
                .0
                .checked_sub(3 - inner_margin.0)
                .unwrap_or_default()
        } else if outer_margin.1 > 0 && inner_margin.1 < 3 {
            outer_margin.0 + (3 - inner_margin.1)
        } else {
            scroll
        };

        *prev_scroll = scroll;

        let p = Paragraph::new(text).scroll((0, scroll));

        let p = if border {
            p.block(
                Block::default()
                    .title(self.title.as_ref())
                    .borders(Borders::ALL),
            )
        } else {
            p.style(Style::default().add_modifier(Modifier::UNDERLINED))
        };

        if self.is_focused {
            if border {
                frame.set_cursor(cur_pos + 1 - scroll, area.y + 1);
            } else {
                frame.set_cursor(cur_pos - scroll, area.y);
            }
        }

        frame.render_widget(p, area);
    }

    fn input(&mut self, msg: Self::Message) {
        if !self.is_focused {
            return;
        }
        match msg {
            Msg::Input(c) => {
                self.text.insert(self.cur_pos, c);
                self.cur_pos += 1;
            }
            Msg::DelWordBackward => {
                let new_pos = self.get_word_start();
                self.text.drain(new_pos..self.cur_pos);
                self.cur_pos = new_pos;
            }
            Msg::DelWordForward => {
                self.text.drain(self.cur_pos..self.get_word_end());
            }
            Msg::MoveWordBackward => {
                let new_pos = self.get_word_start();
                self.cur_pos = new_pos;
            }
            Msg::MoveWordForward => {
                let new_pos = self.get_word_end();
                self.cur_pos = new_pos;
            }
            Msg::Backspace => {
                self.cur_pos = self.cur_pos.checked_sub(1).unwrap_or_default();
                if self.cur_pos < self.text.len() {
                    self.text.remove(self.cur_pos);
                }
            }
            Msg::Del => {
                if self.cur_pos < self.text.len() {
                    self.text.remove(self.cur_pos);
                }
            }
            Msg::Left => {
                self.cur_pos = self.cur_pos.checked_sub(1).unwrap_or_default();
            }
            Msg::Right => {
                if self.cur_pos < self.text.len() {
                    self.cur_pos += 1;
                }
            }
            Msg::Home => {
                self.cur_pos = 0;
            }
            Msg::End => {
                self.cur_pos = self.text.len();
            }
        }
    }

    fn subscribe(&self) -> Vec<Subscription<Self::Message>> {
        vec![]
    }

    fn map_input(&self, event: crossterm::event::Event) -> Option<Self::Message> {
        match event {
            crossterm::event::Event::Key(key) => match (key.modifiers, key.code) {
                (KeyModifiers::CONTROL, KeyCode::Char('w')) => Some(Msg::DelWordBackward),
                (KeyModifiers::ALT, KeyCode::Char('d')) => Some(Msg::DelWordForward),
                (KeyModifiers::CONTROL, KeyCode::Left) => Some(Msg::MoveWordBackward),
                (KeyModifiers::CONTROL, KeyCode::Right) => Some(Msg::MoveWordForward),
                (KeyModifiers::NONE, KeyCode::Backspace) => Some(Msg::Backspace),
                (KeyModifiers::NONE, KeyCode::Left) => Some(Msg::Left),
                (KeyModifiers::NONE, KeyCode::Right) => Some(Msg::Right),
                (KeyModifiers::NONE, KeyCode::Home) => Some(Msg::Home),
                (KeyModifiers::NONE, KeyCode::End) => Some(Msg::End),
                (KeyModifiers::NONE, KeyCode::Delete) => Some(Msg::Del),
                (KeyModifiers::NONE, KeyCode::Char(c)) => Some(Msg::Input(c)),
                (KeyModifiers::NONE, KeyCode::Enter) => None,
                _ => None,
            },
            _ => None,
        }
    }

    fn on_tick(&mut self) -> Option<Self::Message> {
        None
    }

    fn set_focus(&mut self, is_focused: bool) -> bool {
        self.is_focused = is_focused;
        true
    }
}

impl Input {
    fn get_text(&self) -> Text<'_> {
        if self.text.is_empty() {
            Spans::from(vec![Span::styled(
                &self.placeholder,
                Style::default().add_modifier(Modifier::ITALIC),
            )])
            .into()
        } else {
            Span::raw(&self.text).into()
        }
    }

    fn get_word_start(&self) -> usize {
        // `Chars` is not a double-ended iterator
        #[allow(clippy::needless_collect)]
        let iter: Vec<_> = self.text.chars().take(self.cur_pos).collect();
        let iter = iter.into_iter().rev();

        let d = get_word_end_delta(iter);
        self.cur_pos.checked_sub(d).unwrap_or_default()
    }

    fn get_word_end(&self) -> usize {
        let iter = self.text.chars().skip(self.cur_pos + 1);
        min(self.cur_pos + get_word_end_delta(iter) + 1, self.text.len())
    }
}

fn get_word_end_delta<I: Iterator<Item = char>>(iter: I) -> usize {
    let mut found_c = false;
    let mut new_pos = 0;
    let mut l = 0;
    for (i, c) in iter.enumerate() {
        l = i;
        if !c.is_whitespace() {
            found_c = true;
        } else if found_c {
            new_pos = i;
            break;
        }
    }
    if new_pos == 0 {
        new_pos = l + 1;
    }
    new_pos
}
