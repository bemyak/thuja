//! Component that draws a "status bar" listing all available controls

use crossterm::event::{Event, KeyModifiers};
use tui::{
    layout::{Constraint, Direction, Layout},
    style::{Color, Style},
    widgets::Paragraph,
};

use crate::{Component, Subscription};

/// Model for the Legend comp
pub struct Legend<C> {
    inner: C,
}

impl<C> Legend<C>
where
    C: Component,
{
    /// Create a new Legend controller
    pub fn new(child_component: C) -> Self {
        Self {
            inner: child_component,
        }
    }
}

impl<C> Component for Legend<C>
where
    C: Component,
{
    type Message = C::Message;

    fn view(&self, frame: &mut tui::Frame<'_, crate::ThujaBackend>, area: tui::layout::Rect) {
        let subs = self
            .subscribe()
            .into_iter()
            .filter_map(|s| s.description.zip(event_to_str(&s.event)))
            .map(|(desc, event)| format!("{event}:{desc}"))
            .collect::<Vec<_>>()
            .join(" ");
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(1), Constraint::Length(1)].as_ref())
            .split(area);
        self.inner.view(frame, chunks[0]);
        let paragraph = Paragraph::new(subs).style(Style::default().bg(Color::White));
        frame.render_widget(paragraph, chunks[1]);
    }

    fn input(&mut self, msg: Self::Message) {
        self.inner.input(msg)
    }

    fn subscribe(&self) -> Vec<Subscription<Self::Message>> {
        self.inner.subscribe()
    }

    fn on_tick(&mut self) -> Option<Self::Message> {
        self.inner.on_tick()
    }

    fn set_focus(&mut self, is_focused: bool) -> bool {
        self.inner.set_focus(is_focused)
    }

    fn map_input(&self, event: Event) -> Option<Self::Message> {
        self.inner.map_input(event)
    }
}

fn event_to_str(event: &Event) -> Option<String> {
    match event {
        Event::Key(k) => {
            let mut v = vec![];
            if k.modifiers.contains(KeyModifiers::CONTROL) {
                v.push("Ctrl".to_owned());
            }
            if k.modifiers.contains(KeyModifiers::ALT) {
                v.push("Alt".to_owned());
            }
            if k.modifiers.contains(KeyModifiers::SHIFT) {
                v.push("Shift".to_owned());
            }
            match k.code {
                crossterm::event::KeyCode::Backspace => v.push("Backspace".to_owned()),
                crossterm::event::KeyCode::Enter => v.push("Enter".to_owned()),
                crossterm::event::KeyCode::Left => v.push("←".to_owned()),
                crossterm::event::KeyCode::Right => v.push("→".to_owned()),
                crossterm::event::KeyCode::Up => v.push("↑".to_owned()),
                crossterm::event::KeyCode::Down => v.push("↓".to_owned()),
                crossterm::event::KeyCode::Home => v.push("Home".to_owned()),
                crossterm::event::KeyCode::End => v.push("End".to_owned()),
                crossterm::event::KeyCode::PageUp => v.push("PgUp".to_owned()),
                crossterm::event::KeyCode::PageDown => v.push("PgDown".to_owned()),
                crossterm::event::KeyCode::Tab => v.push("Tab".to_owned()),
                crossterm::event::KeyCode::BackTab => {
                    v.push("Shift".to_owned());
                    v.push("Tab".to_owned())
                }
                crossterm::event::KeyCode::Delete => v.push("Del".to_owned()),
                crossterm::event::KeyCode::Insert => v.push("Ins".to_owned()),
                crossterm::event::KeyCode::F(i) => v.push(format!("F{i}")),
                crossterm::event::KeyCode::Char(c) => v.push(c.to_uppercase().to_string()),
                crossterm::event::KeyCode::Null => {}
                crossterm::event::KeyCode::Esc => v.push("Esc".to_owned()),
            }
            if v.is_empty() {
                None
            } else {
                Some(v.join("+"))
            }
        }
        Event::Mouse(_) => None,
        Event::Resize(_, _) => None,
    }
}
