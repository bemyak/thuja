//! Thuja-style wrapper around [tui::widgets::List]

use std::cell::RefCell;

use crossterm::event::{Event, KeyCode, KeyEvent};
use tui::{
    style::{Color, Style},
    text::Text,
    widgets::{List as TuiList, ListItem, ListState},
};

use crate::{Component, Subscription};

/// Messages produced by the List component
#[derive(Debug, PartialEq)]
pub enum Msg {
    /// Move selection up
    Up,
    /// Move selection down
    Down,
}

/// List component that manages a vertical list of elements
pub struct List<'a> {
    state: RefCell<ListState>,
    widget: TuiList<'a>,
    len: usize,
}

impl<'a> Component for List<'a> {
    type Message = Msg;

    fn view(&self, frame: &mut tui::Frame<'_, crate::ThujaBackend>, area: tui::layout::Rect) {
        frame.render_stateful_widget(self.widget.clone(), area, &mut self.state.borrow_mut());
    }

    fn input(&mut self, msg: Self::Message) {
        match msg {
            Msg::Up => self.previous(),
            Msg::Down => self.next(),
        }
    }

    fn subscribe(&self) -> Vec<Subscription<Self::Message>> {
        vec![
            Subscription {
                event: Event::Key(KeyEvent::from(KeyCode::Up)),
                description: Some("Up".to_owned()),
                msg: Msg::Up,
            },
            Subscription {
                event: Event::Key(KeyEvent::from(KeyCode::Down)),
                description: Some("Down".to_owned()),
                msg: Msg::Down,
            },
        ]
    }

    fn on_tick(&mut self) -> Option<Self::Message> {
        None
    }

    fn set_focus(&mut self, is_focused: bool) -> bool {
        let mut tmp = TuiList::new(vec![]);
        std::mem::swap(&mut self.widget, &mut tmp);
        tmp = tmp.highlight_style(if is_focused {
            Style::default().fg(Color::Black).bg(Color::White)
        } else {
            Style::default().fg(Color::Black).bg(Color::Black)
        });
        self.widget = tmp;
        true
    }

    fn map_input(&self, _event: Event) -> Option<Self::Message> {
        None
    }
}

impl<'a> List<'a> {
    /// Create a new list component from an iterator
    pub fn new<T: Into<Text<'a>>, I: IntoIterator<Item = T>>(elements: I) -> Self
    where
        Self: Sized,
    {
        let items: Vec<_> = elements
            .into_iter()
            .map(|item| ListItem::new(item))
            .collect();
        let len = items.len();
        let widget =
            TuiList::new(items).highlight_style(Style::default().fg(Color::Black).bg(Color::Black));
        let state = RefCell::new(ListState::default());
        Self { state, widget, len }
    }

    /// Get the selected index
    pub fn get_selected(&self) -> Option<usize> {
        self.state.borrow().selected()
    }

    fn next(&mut self) {
        let i = match self.state.borrow().selected() {
            Some(i) => {
                if i >= self.len - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.borrow_mut().select(Some(i));
    }

    fn previous(&mut self) {
        let i = match self.state.borrow().selected() {
            Some(i) => {
                if i == 0 {
                    self.len - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.borrow_mut().select(Some(i));
    }
}
