//! Ephemeral component to handle Ctrl+C events

use std::fmt::Display;

use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};

use crate::{Component, ComponentExt, Subscription};

/// Ctrl+C handler's messages
/// You probably want to register [CtrlCMsg::Exit] as an app's exit message using [Thuja::with_exit_msg]
#[derive(PartialEq)]
pub enum CtrlCMsg<M: PartialEq> {
    /// Ctrl+C was pressed
    Exit,
    /// Message of the inner component
    Inner(M),
}

/// CtrlCHandler model
pub struct CtrlCHandler<C, D>
where
    C: Component,
    D: Display,
{
    inner: C,
    description: D,
}

impl<C, D> CtrlCHandler<C, D>
where
    C: Component,
    D: Display,
{
    /// Create new Ctrl+C handler
    pub fn new(child_component: C, description: D) -> Self {
        Self {
            inner: child_component,
            description,
        }
    }
}

impl<C, D> Component for CtrlCHandler<C, D>
where
    C: Component,
    D: Display,
{
    type Message = CtrlCMsg<C::Message>;

    fn view(&self, frame: &mut tui::Frame<'_, crate::ThujaBackend>, area: tui::layout::Rect) {
        self.inner.view(frame, area)
    }

    fn input(&mut self, msg: Self::Message) {
        match msg {
            CtrlCMsg::Exit => unreachable!(),
            CtrlCMsg::Inner(msg) => self.inner.input(msg),
        }
    }

    fn subscribe(&self) -> Vec<Subscription<Self::Message>> {
        vec![Subscription {
            event: Event::Key(KeyEvent {
                code: KeyCode::Char('c'),
                modifiers: KeyModifiers::CONTROL,
            }),
            description: Some(self.description.to_string()),
            msg: Self::Message::Exit,
        }]
        .into_iter()
        .chain(self.inner.map_subscriptions(CtrlCMsg::Inner))
        .collect()
    }

    fn set_focus(&mut self, is_focused: bool) -> bool {
        self.inner.set_focus(is_focused)
    }

    fn on_tick(&mut self) -> Option<Self::Message> {
        self.inner.on_tick().map(CtrlCMsg::Inner)
    }

    fn map_input(&self, event: Event) -> Option<Self::Message> {
        self.inner.map_input(event).map(CtrlCMsg::Inner)
    }
}
