use crossterm::event::Event;
use tui::{layout::Rect, Frame};

use crate::{Subscription, ThujaBackend};

/// Something that can be rendered
pub trait Component {
    /// Message that the component is using
    type Message: PartialEq;

    /// Render the component to the specified frame. The component should fit in the specified area rectangle.
    /// `self` needs to be mutable in order to make `tui`'s stateful widgets work.
    fn view(&self, frame: &mut Frame<'_, ThujaBackend>, area: Rect);

    /// Process a message
    fn input(&mut self, msg: Self::Message);

    /// Map of events that this Component can handle and their descriptions.
    fn subscribe(&self) -> Vec<Subscription<Self::Message>>;

    /// Maps unhandled input even to Msg
    /// This is the way to handle arbitrary events
    fn map_input(&self, event: Event) -> Option<Self::Message>;

    /// Update state on each tick (250ms by default)
    fn on_tick(&mut self) -> Option<Self::Message>;

    /// Set or unset a component's focus. Return `true` if the operation is supported and was successful.
    fn set_focus(&mut self, is_focused: bool) -> bool;
}

pub trait ComponentExt: Component {
    /// Helper function to transform `Subscription<M1>` to `Subscription<M2>`
    /// Often used for wrapping children's subscriptions a into parent's one
    fn map_subscriptions<M, F>(&self, f: F) -> Vec<Subscription<M>>
    where
        M: PartialEq,
        F: Fn(Self::Message) -> M,
    {
        self.subscribe().into_iter().map(|s| s.map(&f)).collect()
    }
}

impl<C> ComponentExt for C where C: Component {}
