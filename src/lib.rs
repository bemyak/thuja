#![warn(rust_2018_idioms)]
// #![warn(missing_docs)]
#![doc = include_str!("../README.md")]

use std::io::Stdout;

use tui::backend::CrosstermBackend;

pub mod components;

mod app;
pub use self::app::*;

mod component;
pub use self::component::*;

mod subscription;
pub use self::subscription::*;

/// Terminal backend used by Thuja
pub type ThujaBackend = CrosstermBackend<Stdout>;
