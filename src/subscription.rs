use crossterm::event::Event;

/// Subscription to an event
pub struct Subscription<M> {
    /// Event to subscribe to
    pub event: Event,
    /// Optional description
    /// Can be used by various components, e.g. to display a legend
    pub description: Option<String>,
    /// Message that will be send in case of the event
    pub msg: M,
}

impl<M1: PartialEq> Subscription<M1> {
    /// Wrap subscription message to another message
    pub fn map<M2: PartialEq, F: Fn(M1) -> M2>(self, f: F) -> Subscription<M2> {
        Subscription {
            event: self.event,
            description: self.description,
            msg: f(self.msg),
        }
    }
}
