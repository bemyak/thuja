//! The main application

use crossterm::execute;
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use std::error::Error;
use std::io;
use std::time::{Duration, Instant};
use tui::backend::CrosstermBackend;
use tui::Terminal;

use crossterm::event::{self, DisableMouseCapture, EnableMouseCapture};

use super::{Component, ThujaBackend};

/// The main application structure
pub struct Thuja<C>
where
    C: Component,
{
    entry: C,
    exit_msg: Option<C::Message>,
    tick_rate: Duration,
}

impl<C> Thuja<C>
where
    C: Component,
{
    /// Create a new Thuja application
    pub fn new(entry: C) -> Self {
        Self {
            entry,
            exit_msg: None,
            tick_rate: Duration::from_millis(250),
        }
    }

    /// Specify on which message Thuja should break the internal loop and return the control back.
    pub fn with_exit_msg(mut self, exit_msg: Option<C::Message>) -> Self {
        self.exit_msg = exit_msg;
        self
    }

    /// Specify the refresh rate of your terminal
    pub fn with_tick_rate(mut self, tick_rate: Duration) -> Self {
        self.tick_rate = tick_rate;
        self
    }

    /// Start the Thuja application. This method will block until an `exit_msg` is received.
    pub fn run(&mut self) -> Result<(), Box<dyn Error>> {
        let mut term = Term::new()?;
        let mut last_tick = Instant::now();
        self.entry.set_focus(true);
        'main: loop {
            term.inner.draw(|f| self.entry.view(f, f.size()))?;

            let timeout = self
                .tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or(Duration::ZERO);
            if crossterm::event::poll(timeout)? {
                let event = event::read()?;
                for s in self.entry.subscribe() {
                    if s.event == event {
                        match &self.exit_msg {
                            Some(exit_msg) if exit_msg == &s.msg => break 'main,
                            _ => self.entry.input(s.msg),
                        }
                    }
                }
                if let Some(msg) = self.entry.map_input(event) {
                    self.entry.input(msg);
                }
            }
            if last_tick.elapsed() >= self.tick_rate {
                if let Some(msg) = self.entry.on_tick() {
                    match &self.exit_msg {
                        Some(exit_msg) if exit_msg == &msg => break 'main,
                        _ => self.entry.input(msg),
                    }
                }
                last_tick = Instant::now();
            }
        }
        Ok(())
    }
}

struct Term {
    inner: Terminal<ThujaBackend>,
}

impl Term {
    fn new() -> Result<Self, io::Error> {
        enable_raw_mode()?;
        let mut stdout = io::stdout();
        execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
        let backend = CrosstermBackend::new(stdout);
        let term = Terminal::new(backend)?;
        Ok(Self { inner: term })
    }

    fn disable_raw_mode(&mut self) -> Result<(), io::Error> {
        disable_raw_mode()?;
        execute!(
            self.inner.backend_mut(),
            LeaveAlternateScreen,
            DisableMouseCapture
        )
        .unwrap();
        self.inner.show_cursor()?;
        Ok(())
    }
}

impl Drop for Term {
    fn drop(&mut self) {
        self.disable_raw_mode().unwrap();
    }
}
