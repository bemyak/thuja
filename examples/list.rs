use thuja::{
    components::{
        ctrlc::{CtrlCHandler, CtrlCMsg},
        legend::Legend,
        list::List,
    },
    Thuja,
};

fn main() {
    let list = List::new(vec!["one", "two", "three"]);
    let ctrlc = CtrlCHandler::new(list, "Quit");
    let legend = Legend::new(ctrlc);
    Thuja::new(legend)
        .with_exit_msg(Some(CtrlCMsg::Exit))
        .run()
        .unwrap();
}
