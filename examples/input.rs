use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};
use thuja::{
    components::{
        ctrlc::{CtrlCHandler, CtrlCMsg},
        input::{Input, Msg as InputMsg},
        legend::Legend,
    },
    Component, Thuja,
};
use tui::layout::{Constraint, Direction, Layout};

fn main() {
    let input = InputDemo::new();
    let ctrlc = CtrlCHandler::new(input, "Quit");
    let legend = Legend::new(ctrlc);
    Thuja::new(legend)
        .with_exit_msg(Some(CtrlCMsg::Exit))
        .run()
        .unwrap();
}

#[derive(Debug, PartialEq)]
enum InputDemoMsg {
    InputSmall(InputMsg),
    InputBig(InputMsg),
    SwitchActiveInput,
}

struct InputDemo {
    input_small: Input,
    input_big: Input,
    input_small_focused: Option<bool>,
}

impl InputDemo {
    fn new() -> Self {
        InputDemo {
            input_small: Input::new()
                .with_title("Small input")
                .with_placeholder("placeholder"),
            input_big: Input::new()
                .with_title("Big input")
                .with_placeholder("placeholder"),
            input_small_focused: None,
        }
    }
}

impl Component for InputDemo {
    type Message = InputDemoMsg;

    fn view(&self, frame: &mut tui::Frame<'_, thuja::ThujaBackend>, area: tui::layout::Rect) {
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(1), Constraint::Min(3)].as_ref())
            .split(area);
        self.input_small.view(frame, chunks[0]);
        self.input_big.view(frame, chunks[1]);
    }

    fn input(&mut self, msg: Self::Message) {
        match msg {
            InputDemoMsg::InputSmall(msg) => self.input_small.input(msg),
            InputDemoMsg::InputBig(msg) => self.input_big.input(msg),
            InputDemoMsg::SwitchActiveInput => {
                if let Some(is_input_small_focused) = self.input_small_focused {
                    let is_input_small_focused = !is_input_small_focused;
                    self.input_small_focused = Some(is_input_small_focused);
                    self.input_small.set_focus(is_input_small_focused);
                    self.input_big.set_focus(!is_input_small_focused);
                }
            }
        }
    }

    fn subscribe(&self) -> Vec<thuja::Subscription<Self::Message>> {
        vec![thuja::Subscription {
            event: Event::Key(KeyEvent::new(KeyCode::Tab, KeyModifiers::NONE)),
            description: Some("Switch input".to_string()),
            msg: InputDemoMsg::SwitchActiveInput,
        }]
    }

    fn map_input(&self, event: crossterm::event::Event) -> Option<Self::Message> {
        if self.input_small_focused? {
            self.input_small
                .map_input(event)
                .map(InputDemoMsg::InputSmall)
        } else {
            self.input_big.map_input(event).map(InputDemoMsg::InputBig)
        }
    }

    fn on_tick(&mut self) -> Option<Self::Message> {
        None
    }

    fn set_focus(&mut self, is_focused: bool) -> bool {
        if is_focused {
            self.input_small_focused = Some(true);
            self.input_small.set_focus(true);
            self.input_big.set_focus(false);
        } else {
            self.input_small_focused = None;
            self.input_small.set_focus(false);
            self.input_big.set_focus(false);
        }

        true
    }
}
