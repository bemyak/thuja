# Thuja

Elm-inspired library for building Text User Interfaces.

Thuja heavily relies on the [tui](https://crates.io/crates/tui) crate and essentially is just an (opinionated) wrapper around it.
Thuja uses [crossterm](https://crates.io/crates/crossterm) as a terminal backend.

> Thuja is an experimental library and its API is likely a subject to change

## Quick start

The simplest app looks like this:

```rust,no_run
use thuja::{components::list::List, Component, Thuja};

fn main() {
    let list = List::new(vec!["one", "two", "three"]);
    Thuja::new(list).run();
}
```

This code will display a list of three elements, with the first one selected. You can switch between elements using arrow keys.  
Pretty simple, right?

> The goal of the project is to make components easily reusable and composable.

The next example will display the same list, but with a legend status bar and `Ctrl+C` handling:

```rust,no_run
use thuja::{components::{ctrlc::{CtrlCHandler,CtrlCMsg},legend::Legend,list::List},Thuja};

fn main() {
    let list = List::new(vec!["one", "two", "three"]);
    let ctrlc = CtrlCHandler::new(list, "Quit");
    let legend = Legend::new(ctrlc);
    Thuja::new(legend).with_exit_msg(Some(CtrlCMsg::Exit)).run();
}
```

See the [docs](https://docs.rs/thuja) for further information.

## Why "thuja"?

As said, it is [Elm](https://elm-lang.org)-inspired and the name is consonant with "TUI" (text user interface). Got it?
